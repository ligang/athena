#include "TrigHypoCommonTools/L1InfoHypo.h"
#include "TrigHypoCommonTools/L1InfoHypoTool.h"
#include "TrigHypoCommonTools/TrigComboHypoTool.h"

DECLARE_COMPONENT(L1InfoHypo)
DECLARE_COMPONENT(L1InfoHypoTool)
DECLARE_COMPONENT(TrigComboHypoTool)
